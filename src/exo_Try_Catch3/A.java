package exo_Try_Catch3;
import exo_Try_Catch2.Erreur;

public class A {

	public A(int n) throws Erreur {
		if (n == 1)
			throw new Erreur();
	}
}
