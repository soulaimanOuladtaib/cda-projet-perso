package exo_Try_Catch5;

import java.util.Scanner;

public class EntNat {
	private int entier;

	public EntNat(int entier) throws ErrConst {
		if (entier < 0) {
			ErrConst e = new ErrConst(entier);
			throw e;
		} else {
			this.entier = entier;
		}

	}

	public int getN() {
		return this.entier;
	}

	public static EntNat somme(EntNat e1, EntNat e2) throws ErrSom, ErrConst {
		int et = e1.entier;
		int etr = e2.entier;
		long res = (long) et + (long) etr;
		if (res > Integer.MAX_VALUE) {
			throw new ErrSom(e1.entier, e2.entier);
		}
		return new EntNat(et + etr);
	}

	public static EntNat difference(EntNat e3, EntNat e4) throws ErrDiff, ErrConst {

		int et = e3.entier;
		int etr = e4.entier;
		int res = et - etr;
		if (res < 0) {
			throw new ErrDiff(e3.entier, e4.entier);
		}
		return new EntNat(et - etr);
	}

	public static EntNat produit(EntNat e5, EntNat e6) throws ErrProd, ErrConst {
		 int et = e5.entier;
	        int etr = e6.entier;
	        long res = (long) et * (long) etr;
	        if (res > Integer.MAX_VALUE) {
	            throw new ErrProd(e5.entier, e6.entier);
	        }
	        return new EntNat(et * etr);
	    }
	

	public static void main(String[] args) throws ErrConst {

		
	    Scanner sc = new Scanner(System.in);
        System.out.println("Saisissez un entier positif : ");
        try {
            EntNat a = new EntNat(sc.nextInt());
            System.out.println("Saisissez un entier positif : ");
            EntNat b = new EntNat(sc.nextInt());
            EntNat res = EntNat.somme(a, b);
            EntNat res2 = EntNat.difference(a, b);
            EntNat res3 = EntNat.produit(a, b);
        } catch (ErrNat e) {
            System.out.println(e.getMessage());
        }
        System.out.println("suite du programme ***************************");
        System.out.println("Saisissez un entier positif : ");
        try {
            EntNat a = new EntNat(sc.nextInt());
            System.out.println("Saisissez un entier positif : ");
            EntNat b = new EntNat(sc.nextInt());
            EntNat res = EntNat.somme(a, b);
            EntNat res2 = EntNat.difference(a, b);
            EntNat res3 = EntNat.produit(a, b);
        } catch (ErrConst | ErrSom | ErrDiff | ErrProd e) {
            System.out.println(e.getMessage());
        }
    
		
		
		
		
		
	}

}
